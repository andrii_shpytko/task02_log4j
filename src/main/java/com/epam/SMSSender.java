package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSSender {

    public static final String ACCOUNT_SID = "ACa7ba7b843b43a6fe7c74dd70b4eff578";
    public static final String AUTH_TOKEN = "ab11028b27ee2d88973c14cbadba6fdc";

    public static void send(String str){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380677977349"),
                new PhoneNumber("+12563673804"), str)
                .create();
    }
}
